import {createContext, useState, createRef} from 'react';

export const FilmContext = createContext();

export const FilmProvider = ({children}) => {
    const [playerRef, setPlayerRef] = useState(createRef());

    return (
        <FilmContext.Provider value={{playerRef}}>
            {children}
        </FilmContext.Provider>
    );
}