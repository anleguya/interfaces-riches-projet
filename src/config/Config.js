export const backendUrl = 'https://imr3-react.herokuapp.com/backend';
export const webSocketUrl = 'wss://imr3-react.herokuapp.com/ws';