function findLast(array, predicate) {
    const arrayFiltered = array.filter(predicate);
    return arrayFiltered[arrayFiltered.length - 1];
}

export {findLast};