import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import FilmComponent from './FilmComponent';
import { mockFilm } from '../../config/ConfigTest';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <FilmComponent film={mockFilm} />
        </FilmProvider>
    );
});