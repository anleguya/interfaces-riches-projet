import React, {useContext} from 'react';
import {Player} from 'video-react';
import "../../../node_modules/video-react/dist/video-react.css";
import {FilmContext} from '../../context/FilmContext';
import "../../css/FilmComponent.css";

function FilmComponent(props) {
    const film = props.film;
    const {playerRef} = useContext(FilmContext);

    return (
        <div className={props.className}>
            <h1>Film</h1>
            <Player
                ref={playerRef}
                playsInline
                poster="/assets/poster.png"
                src={film.file_url}
            />
            <h1>{film.title}</h1>
            <div className='film-description'>
                <h3>Description</h3>
                <div className='film-info'>
                    <a href={film.synopsis_url}>{film.synopsis_url}</a>
                </div>
            </div>
        </div>
    );
}

export default FilmComponent;