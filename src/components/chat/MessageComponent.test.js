import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import {messageExample, messageWithMomentExample} from '../../config/ConfigTest';
import MessageComponent from './MessageComponent';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <MessageComponent message={messageExample} />
        </FilmProvider>
    );
});

test("Renders moment without crashing", () => {
    render(
        <FilmProvider>
            <MessageComponent message={messageWithMomentExample} />
        </FilmProvider>
    );
});