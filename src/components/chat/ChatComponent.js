import React, {useEffect, useState} from 'react';
import ChatDisplayComponent from './ChatDisplayComponent';
import ChatPostingComponent from './ChatPostingComponent';
import {webSocketUrl} from '../../config/Config';

function ChatComponent(props) {
    const [webSocketConnected, setWebSocketConnected] = useState(false);
    const [webSocket, setWebSocket] = useState(null);

    useEffect(() => {
        // Connexion au WebSocket si celui-ci n'est pas déjà connecté
        if (!webSocketConnected) {
            const webSocket = new WebSocket(webSocketUrl);
            webSocket.onopen = () => {
                setWebSocketConnected(true);
                setWebSocket(webSocket);
            }
            webSocket.onerror = () => {
                setWebSocketConnected(false);
            }
            webSocket.onclose = () => {
                setWebSocketConnected(false);
            }
        }
    }, [webSocketConnected]);

    return (
        <div className={props.className}>
            <h2>Chat</h2>
            {webSocket !== null && (
                <div>
                    <ChatDisplayComponent socket={webSocket}/>
                    <ChatPostingComponent socket={webSocket}/>
                </div>
            )}
            
        </div>
    );
}

export default ChatComponent;