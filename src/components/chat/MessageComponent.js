import React from 'react';

function MessageComponent(props) {
    const dateMessage = new Date(props.message.when);
    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

    const formatDate = (date) => {
        const day = date.getDate();
        const month = monthNames[date.getMonth()];
        const year = date.getFullYear();
        const hours = date.getHours();
        const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
        return `${day} of ${month} ${year} at ${hours}:${minutes}`;
    }
    
    return (
        <div>
            <span><b>{props.message.name}: </b></span>
            <span>{props.message.message} </span>
            { props.message.moment != null && 
                <button style={{display: "inline"}} onClick={() => props.playerRef.current.seek(props.message.moment)}>Go to moment</button>
            }
            <p>{formatDate(dateMessage)}</p>
        </div>
    );
}

export default MessageComponent;