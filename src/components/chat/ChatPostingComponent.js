import React, {useState, useContext, useEffect} from 'react';
import {FilmContext} from '../../context/FilmContext';
import '../../css/ChatPostingComponent.css';

function ChatPostingComponent(props) {
    const name = "Gaël & Antho";
    const [message, setMessage] = useState('');
    const {playerRef} = useContext(FilmContext);
    const [currentTime, setCurrentTime] = useState(0);

    const onMessageSubmit = (event) => {
        event.preventDefault();
        const messageToSend = event.target[0].value;
        const shareMoment = event.target[2].checked;
        setMessage('');
        if (props != null && props.socket != null) {
            const messageToSendJson = JSON.stringify(shareMoment ? 
                {name: name, message: messageToSend, moment: currentTime} : 
                {name: name, message: messageToSend});
            props.socket.send(messageToSendJson);
        }
    }
    
    useEffect(() => {
        playerRef.current?.subscribeToStateChange((state) => {
            setCurrentTime(state.currentTime);
        });
    }, [playerRef]);

    return (
        <div className='chatPostingContainer'>
            <h2>Chat Posting</h2>
            <div className='chatInputContainer'>
                <form onSubmit={onMessageSubmit}>
                    <input type='text'
                            value={message}
                            placeholder='Message'
                            onChange={(e) => setMessage(e.target.value)}/>
                    <button type='submit'>Send</button>
                    <input type="checkbox" id="shareMoment" name="shareMoment" value="shareMoment"/> Share moment 
                </form>
            </div>
        </div>
    );
}

export default ChatPostingComponent;