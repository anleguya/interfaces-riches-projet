import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import ChatComponent from './ChatComponent';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <ChatComponent/>
        </FilmProvider>
    );
});