import React, {useEffect, useState, useContext} from 'react';
import MessageComponent from './MessageComponent';
import {FilmContext} from '../../context/FilmContext';
import '../../css/ChatDisplayComponent.css';

function ChatDisplayComponent(props) {
    const [messages, setMessages] = useState([]);
    const {playerRef} = useContext(FilmContext);

    const addMessages = (messagesReceived) => {
        setMessages((previousMessages) => [...previousMessages, ...messagesReceived]);
    }

    const onMessage = (event) => {
        const messagesReceived = JSON.parse(event.data);

        // On vire Rick (il est trop fort)
        const messagesReceivedFiltered = messagesReceived.filter(message => 
            message.name !== 'Rick' && message.name !== 'dsqdq');
        if (messagesReceivedFiltered !== null && messagesReceivedFiltered.length > 0) {
            addMessages(messagesReceivedFiltered);
        }
    }

    useEffect(() => {
        if (props.socket != null) {
            props.socket.onmessage = onMessage;
        }
    }, []);

    return (
        <div className='display'>
            <h2>Chat Display</h2>
            <div>
                {messages.map((message, index) => {
                    return (
                        <MessageComponent key={index} message={message} playerRef={playerRef}/>
                    );
                })}
            </div>
        </div>
    );
}

export default ChatDisplayComponent;