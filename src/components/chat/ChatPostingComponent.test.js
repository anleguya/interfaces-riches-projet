import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import ChatPostingComponent from './ChatPostingComponent';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <ChatPostingComponent/>
        </FilmProvider>
    );
});