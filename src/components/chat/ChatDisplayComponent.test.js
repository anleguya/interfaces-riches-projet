import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import ChatDisplayComponent from './ChatDisplayComponent';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <ChatDisplayComponent/>
        </FilmProvider>
    );
});