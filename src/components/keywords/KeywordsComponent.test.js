import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import KeywordsComponent from './KeywordsComponent';
import { mockKeywords } from '../../config/ConfigTest';

test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <KeywordsComponent keywords={mockKeywords} />
        </FilmProvider>
        );
});