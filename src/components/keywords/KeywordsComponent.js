import React, {useEffect, useState, useContext} from 'react';
import {FilmContext} from '../../context/FilmContext';
import "../../css/KeywordsComponent.css";
import {findLast} from '../../utils/Utils';

function KeywordsComponent(props) {
    const keywords = props.keywords;
    const {playerRef} = useContext(FilmContext);
    const [currentTime, setCurrentTime] = useState(0);
    const [currentKeywords, setCurrentKeywords] = useState(null);

    useEffect(() => {
        playerRef.current?.subscribeToStateChange((state) => {
            setCurrentTime(state.currentTime);
        });
    }, [playerRef]);

    useEffect(() => {
        
        const keywordsFound = findLast(keywords, kw => kw.pos <= currentTime);
        
       if (keywordsFound.pos !== currentKeywords?.pos) {
        setCurrentKeywords(keywordsFound);
    }
    }, [currentKeywords, currentTime, keywords]);

    const renderCurrentKeywords = () => {
        return (
            currentKeywords?.data.map(keyword => 
                <a href={keyword.url} target="_blank" rel="noreferrer" key={keyword.title}><li className='keywords-list-element'>{keyword.title}</li></a>
            )
        )
    };

    return (
        <div className={props.className}>
            <h2>Keywords</h2>
            <ul className='keywords-list'>
                {renderCurrentKeywords()}
            </ul>
        </div>
    );
}

export default KeywordsComponent;