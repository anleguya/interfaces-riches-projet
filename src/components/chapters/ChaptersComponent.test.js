import { render, waitForElement } from '@testing-library/react';
import { FilmProvider } from '../../context/FilmContext';
import {mockChapters} from '../../config/ConfigTest';
import ChaptersComponent from './ChaptersComponent';
import {ProSidebarProvider} from 'react-pro-sidebar';

test("Renders without crashing", () => {
    render(
        <ProSidebarProvider>
            <FilmProvider>
                <ChaptersComponent chapters={mockChapters} />
            </FilmProvider>
        </ProSidebarProvider>
    );
});