import React, {useContext} from 'react';
import '../../css/ChaptersComponent.css';
import {FilmContext} from '../../context/FilmContext';
import {Sidebar, Menu, MenuItem, useProSidebar} from 'react-pro-sidebar';
//import icons from react icons
import {FaBars} from "react-icons/fa";
import {FiArrowRightCircle} from "react-icons/fi";

function ChaptersComponent(props) {
    const chapters = props.chapters;
    const {collapseSidebar, collapsed} = useProSidebar();
    const {playerRef} = useContext(FilmContext);

    const renderChapterMenuItem = (chapter) => {
        if (chapter !== null) {
            if (collapsed) {
                return (
                    <MenuItem key={chapter.pos}/>
                );
            }
            else {
                return (
                    <MenuItem key={chapter.pos} onClick={() => {
                        playerRef.current.seek(chapter.pos);
                    }} icon={<FiArrowRightCircle />}>{chapter.title}</MenuItem>
                );
            }
        }
        else {
            return null;
        }
    }

    return (
        <div className={props.className}>
            <main className='collapseButtonWrapper'>
                <button className='collapseButton' onClick={() => collapseSidebar()}><FaBars size={30}/></button>
                { !collapsed && <h1>Chapters</h1> }
            </main>
            <Sidebar>
                <Menu iconShape="square">
                    {
                        chapters !== null && chapters.length > 0
                        ? chapters.map(chapter =>
                            renderChapterMenuItem(chapter)
                        )
                        :
                        null
                    }
                </Menu>
            </Sidebar>
        </div>
    );
}

export default ChaptersComponent;