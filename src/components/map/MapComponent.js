import React, {useContext, useEffect, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import {FilmContext} from '../../context/FilmContext';
import {findLast} from '../../utils/Utils';

function MapComponent(props) {
    const waypoints = props.waypoints;
    const {playerRef} = useContext(FilmContext);
    const [currentTime, setCurrentTime] = useState(0);
    const [markersOpacity, setMarkersOpacity] = useState(waypoints.map(_ => 0.5));
    const bounds = waypoints.map(waypoint => [waypoint.lat, waypoint.lng]);

    useEffect(() => {
        playerRef.current?.subscribeToStateChange((state) => {
            setCurrentTime(state.currentTime);
        });
    }, [playerRef]);

    useEffect(() => {
        const currentWaypoint = findLast(waypoints, (wp) => wp.timestamp <= currentTime);

        if (currentWaypoint) {
            // There can be multiple current waypoints for the same timestamp
            const currentWaypointsIndices = waypoints
                .map((wp, i) => wp.timestamp === currentWaypoint.timestamp ? i : -1)
                .filter(i => i !== -1);

            let newMarkersOpacity = waypoints.map(_ => 0.5);

            for (const i in currentWaypointsIndices) {
                newMarkersOpacity[currentWaypointsIndices[i]] = 1;
            }

            setMarkersOpacity(newMarkersOpacity);
        }
    }, [currentTime, waypoints]); 
    
    const renderMarkers = () => {
        return (
            waypoints.map((waypoint, index) =>
                <Marker
                    key={waypoint.label}
                    position={[waypoint.lat, waypoint.lng]}
                    opacity={markersOpacity[index]}
                    eventHandlers={{
                        click: (e) => {
                            playerRef.current.seek(waypoint.timestamp);
                        }
                    }}>
                    <Popup>
                        {waypoint.label}
                    </Popup>
                </Marker>
            )
        )
    }

    return (
        <div id="map" className={props.className}>
            <MapContainer bounds={bounds} zoom={13} scrollWheelZoom={false} >
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {renderMarkers()}
            </MapContainer>
        </div>
    );
}

export default MapComponent;