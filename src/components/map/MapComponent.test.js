import { render, screen } from '@testing-library/react';
import { mockWaypoints } from '../../config/ConfigTest';
import { FilmProvider } from '../../context/FilmContext';
import MapComponent from './MapComponent';


test("Renders without crashing", () => {
    render(
        <FilmProvider>
            <MapComponent waypoints={mockWaypoints} />
        </FilmProvider>
    );
});

test("Markers are rendered", () => {
    render(
        <FilmProvider>
            <MapComponent waypoints={mockWaypoints} />
        </FilmProvider>
    );
    
    const markers = screen.getAllByRole("button");
    expect(markers.length).toBeGreaterThan(2); // There are '+' and '-' buttons before markers
});

test("There are exactly 8 markers", () => {
    render(
        <FilmProvider>
            <MapComponent waypoints={mockWaypoints} />
        </FilmProvider>
    );

    const markers = screen.getAllByRole("button");
    expect(markers).toHaveLength(8 + 2); // There are 2 more buttons for '+' and '-'
});