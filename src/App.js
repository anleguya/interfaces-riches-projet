import './css/App.css';
import React, {useState, useEffect} from 'react';
import ChaptersComponent from './components/chapters/ChaptersComponent';
import FilmComponent from './components/film/FilmComponent';
import MapComponent from './components/map/MapComponent';
import ChatComponent from './components/chat/ChatComponent';
import KeywordsComponent from './components/keywords/KeywordsComponent';
import {backendUrl} from './config/Config';
import { FilmProvider } from './context/FilmContext';
import { ProSidebarProvider } from 'react-pro-sidebar';

function App() {
  const [fetchedData, setFetchedData] = useState(null);
  const [timestamp, setTimestamp] = useState(0);

  useEffect(() => {
    async function fetchData() {
        const response = await fetch(backendUrl);
        const responseJson = await response.json();
        setFetchedData({
          film: responseJson.Film,
          chapters: responseJson.Chapters,
          waypoints: responseJson.Waypoints,
          keywords: responseJson.Keywords
        });
    }
    fetchData();
  }, []);

  if (fetchedData == null) {
    return (
      <div>Loading...</div>
    )
  }

  return (
    <ProSidebarProvider>
      <FilmProvider>
          <main className="app-grid">
            <ChaptersComponent chapters={fetchedData.chapters} className="chapters-component"/>
            <FilmComponent film={fetchedData.film} className="film-component"/>
            <MapComponent waypoints={fetchedData.waypoints} className="map-component"/>
            <KeywordsComponent keywords={fetchedData.keywords} className="keywords-component"/>
            <ChatComponent className="chat-component"/>
          </main>
      </FilmProvider>
    </ProSidebarProvider>
  );
}

export default App;
